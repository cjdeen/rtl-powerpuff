import { FETCH_EPISODES } from '../actions/episodes';

export default (state = [], action) => {
  switch (action.type) {

    case FETCH_EPISODES:
      return action.data;

    default:
      return state;
  }
};
