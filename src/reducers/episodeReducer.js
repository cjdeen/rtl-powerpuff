import { SET_ACTIVE_EPISODE } from '../actions/episode';

export default (state = null, action) => {
  switch (action.type) {

    case SET_ACTIVE_EPISODE:
      return action.data;

    default:
      return state;
  }
};
