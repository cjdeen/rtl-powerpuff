import {  FETCH_SHOW } from '../actions/show';

export default (state = false, action) => {
  switch (action.type) {

    case FETCH_SHOW:
      return action.data;

    default:
      return state;
  }
};
