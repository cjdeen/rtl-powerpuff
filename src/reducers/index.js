// index.js

import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router'

import showReducer from "./showReducer";
import episodesReducer from "./episodesReducer";
import episodeReducer from "./episodeReducer";

export default history => combineReducers({
	router: connectRouter(history),
	show: showReducer,
	episodes: episodesReducer,
	activeEpisode: episodeReducer
})
