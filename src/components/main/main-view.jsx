
import * as React from "react";
import Container from '@material-ui/core/Container';
import { ThemeProvider } from '@material-ui/styles';
import { createMuiTheme } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';

const theme = createMuiTheme({
	palette: {
		primary: blue,
	},
});

const MainView = ({ children }) => (
	<ThemeProvider theme={theme}>
		<Container fixed>
			{children}
		</Container>
	</ThemeProvider>
);

export default MainView;
