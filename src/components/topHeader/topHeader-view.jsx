import * as React from "react";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

export default class TopHeaderView extends React.Component{

	render() {
		if (!this.props) {
			return (<div></div>)
		} else {
			const { image, name, summary } = this.props;
			const sImage = image === null ? "https://via.placeholder.com/300x300?text=Placeholder" : image.medium;
			const sSummary = summary === null ? "" : summary;

			return (
				<Grid container className="topHeader">
					<Grid item md={4}>
						<img alt="powerpuff" src={ sImage } className="img-responsive" />
					</Grid>
					<Grid item md={8}>
						<Typography component="h1" variant="h3" color="inherit" gutterBottom>
							{ name }
						</Typography>
						<Typography paragraph>
							{ sSummary.replace(/<\/?[^>]+>/gi, '') }
						</Typography>
					</Grid>
				</Grid>
			)
		}
	}
}
