import * as React from "react";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import moment from "moment"
import TableContainer from "@material-ui/core/TableContainer";

export default class EpisodeListView extends React.Component{

	render() {
		if (!this.props.episodes) {
			return (<div></div>)
		} else {
			const { episodes } = this.props;

			return (
				<TableContainer component={Paper} className="episodeList">
					<Table aria-label="episode list table">
						<TableHead>
							<TableRow>
								<TableCell>Season</TableCell>
								<TableCell>Episode</TableCell>
								<TableCell>Name of episode</TableCell>
								<TableCell>Released</TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{episodes.map(oEpisode => (
								<TableRow hover onClick={() => this.props.onClickItem(oEpisode)} key={oEpisode.name}>
									<TableCell>{oEpisode.season}</TableCell>
									<TableCell>{oEpisode.number}</TableCell>
									<TableCell component="th" scope="row">
										{oEpisode.name}
									</TableCell>
									<TableCell align="right">{moment(oEpisode.airdate).format('DD/MM/YYYY')}</TableCell>
								</TableRow>
							))}
						</TableBody>
					</Table>
				</TableContainer>
			)
		}
	}
}
