import { bindActionCreators, compose } from 'redux'
import {connect, } from "react-redux";
import { withRouter } from 'react-router';
// import { fetchEpisodes } from "../../actions/episodes";
import {setActiveEpisode} from "../../actions/episode";

//components
import EpisodeList from "./episodeList-container";

export default withRouter(compose(
	connect(
		//mapStateToProps
		store => ({
			store
		}),
		// matchDispatchToProps
		dispatch => bindActionCreators({
			// if an action is required we do it here.
			setActiveEpisode
		}, dispatch)
	)
)(EpisodeList));
