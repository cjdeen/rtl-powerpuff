import * as React from "react";

//components
import EpisodeListView from "./episodeList-view";
import {getUrl} from "../../helpers/url";

export default class EpisodeList extends React.Component {

	constructor(props) {
		super(props);
		this.onClickItem = this.onClickItem.bind(this);
	}

	onClickItem(item) {
		this.props.setActiveEpisode(item);
		this.props.history.push(`/${getUrl(item.url)}`);
	}

	render() {
		return (<EpisodeListView onClickItem={this.onClickItem} episodes={this.props.episodes} />);
	}
}
