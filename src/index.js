import React from 'react';
import ReactDOM from 'react-dom';

import * as serviceWorker from './serviceWorker';
import AppRouter from "./routes";
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import store, { history } from './store';
import {ConnectedRouter} from "connected-react-router";
import { fetchShow  } from "./actions/show"
import { fetchEpisodes  } from "./actions/episodes"
import axios from "axios";


axios.get("https://api.tvmaze.com/shows/6771")
	.then(response => {
		store.dispatch(fetchShow(response.data));
	})
	.catch(error => {
		throw(error);
	});

axios.get("https://api.tvmaze.com/shows/6771/episodes")
	.then(response => {
		store.dispatch(fetchEpisodes(response.data));
	})
	.catch(error => {
		throw(error);
	});

ReactDOM.render(
	<Provider store={store}>
		<ConnectedRouter history={history}>
			<BrowserRouter>
				<AppRouter />
			</BrowserRouter>
		</ConnectedRouter>
	</Provider>,
	document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
