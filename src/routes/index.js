
import { Main } from '../components/main';
import Show from '../pages/show';
import Episode from '../pages/episode';

import React from 'react';
import { Route, Switch  } from 'react-router' // react-router v4/v5

const AppRouter = () => (
	<Main>
		<Switch>
			<Route exact path="/" component={Show} />
			<Route exact path="/:episode" component={Episode} />
		</Switch>
	</Main>
);

export default AppRouter;
