export const SET_ACTIVE_EPISODE = 'SET_ACTIVE_EPISODE';

export const setActiveEpisode = (data) => ({
  type: SET_ACTIVE_EPISODE,
  data: data
});
