export const FETCH_SHOW = 'FETCH_SHOW';

export const fetchShow = (data) => ({
  type: FETCH_SHOW,
  data: data
});
