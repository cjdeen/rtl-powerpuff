export const FETCH_EPISODES = 'FETCH_EPISODES';

export const fetchEpisodes = (data) => ({
  type: FETCH_EPISODES,
  data: data
});
