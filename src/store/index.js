// middleware stuff
import thunk from 'redux-thunk';
import promise from 'redux-promise';
// log the changed state
import { createLogger } from 'redux-logger';

import { routerMiddleware } from 'connected-react-router'
import { createBrowserHistory } from "history";
import { applyMiddleware, createStore } from "redux";
import createRootReducer from "../reducers";

const logger = createLogger();
export const history = createBrowserHistory();

let middleware = [routerMiddleware(history), thunk, promise];
if (process.env.NODE_ENV !== 'production') {
	middleware = [...middleware, logger]
}

const store = createStore(
	createRootReducer(history),
	applyMiddleware(...middleware)
);

export default store;
