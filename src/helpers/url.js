/**
 * Gets the value of the string after the last "/"
 * @param sInput
 */
export const getUrl = (sInput) => {
	let lastSlashIndex = sInput.lastIndexOf('/');
	return sInput.substring(lastSlashIndex + 1);
};
