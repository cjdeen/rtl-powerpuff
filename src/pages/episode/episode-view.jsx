import * as React from "react";
import TopHeader from "../../components/topHeader";
import EpisodeList from "../../components/episodeList";

export default class EpisodeView extends React.Component{

	render() {
		if (!this.props.episode) {
			return (<h5>Loading...</h5>)
		} else {
			return (
				<TopHeader {...this.props.episode} />
			)
		}
	}
}
