import * as React from "react";

//components
import EpisodeView from "./episode-view";
import { getUrl } from "../../helpers/url";

export default class Episode extends React.Component {

	constructor(props) {
		super(props);
	}

	// when a page is reloaded we still want to show the active episode
	componentDidUpdate(prevProps, prevState, snapshot) {
		if((prevProps.store.episodes !== this.props.store.episodes)) {
			this.props.store.episodes.map((oEpisode) => {
				if (getUrl(oEpisode.url) === this.props.match.params.episode) {
					this.props.setActiveEpisode(oEpisode);
				}
			});
		}
	}

	render() {
		// console.error(this.state.activeEpisode)
		if(!this.props.store.activeEpisode) {
			return (<div></div>);
		} else {
			return (<EpisodeView episode={this.props.store.activeEpisode} />);
		}

	}
}
