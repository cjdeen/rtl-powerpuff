import * as React from "react";

//components
import ShowView from "./show-view";


export default class Show extends React.Component {

	render() {
		return (<ShowView show={this.props.store.show} {...this.props} {...this.state} />);
	}
}
