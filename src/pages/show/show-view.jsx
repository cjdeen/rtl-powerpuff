import * as React from "react";
import TopHeader from "../../components/topHeader";
import EpisodeList from "../../components/episodeList";

export default class ShowView extends React.Component{

	render() {
		if (!this.props.show) {
			return (<h5>Loading...</h5>)
		} else {
			// const { name, summary, image } = this.props.show;
			const { episodes } = this.props.store;

			return (
				<React.Fragment>
					<TopHeader {...this.props.show} />
					<EpisodeList episodes={episodes} />
				</React.Fragment>
			)
		}
	}
}
